# Illustrated WFTDA casebook

This is a repository for vectorized illustrations of situations described in the WFTDA 2023 Rules of Roller Derby Casebook.

## Licence

All of these illustrations can be re-used free of charge, provided you give proper attribution and share them under similar conditions, following the guidelines of the CC-BY-SA 4.0 license.

When re-using, please credit Moutmout/Blocktopus.

## Contributing

This project is not currently looking for contributors. But if you have an awesome idea related to the illustrated WFTDA casebook, please do reach out.

## Roadmap

Future developments may include:

- Illustrations of other scenarios described by the WFTDA casebook.
- A printable version of all the scenarios.
- Illustration of scenarios that are not in the WFTDA casebook, but that hold pedagogical value.
